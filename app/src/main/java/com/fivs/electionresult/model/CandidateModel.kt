package com.fivs.electionresult.model

import java.io.Serializable

open class CandidateModel:Serializable {
    var id : String = ""
    var image : String = ""
    var name : String = ""
    var logo : String = ""
    var state : String = ""
    var stateid : String = ""
    var constituency : String = ""
    var constituencyid : String = ""
    var party : String = ""
    var partyimage : String = ""
    var partyid : String = ""
    var vote : Int = 0
}