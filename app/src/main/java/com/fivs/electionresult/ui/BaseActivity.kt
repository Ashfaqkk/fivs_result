package com.fivs.electionresult.ui

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.fivs.electionresult.R


abstract class BaseActivity : AppCompatActivity() {

    lateinit var mProgressDialog: ConstraintLayout
    lateinit var toolbar: Toolbar
    private lateinit var progressdialog: ProgressDialog
    private lateinit var messageDialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun setContentView(layoutResID: Int) {

        val coordinatorLayout: CoordinatorLayout =
            layoutInflater.inflate(R.layout.activity_base, null) as CoordinatorLayout
        val activityContainer: FrameLayout = coordinatorLayout.findViewById(R.id.layout_container)
        mProgressDialog = coordinatorLayout.findViewById(R.id.clPB) as ConstraintLayout
        layoutInflater.inflate(layoutResID, activityContainer, true)
        mProgressDialog.visibility = View.GONE
        toolbar = coordinatorLayout.findViewById(R.id.toolbar)

        setSupportActionBar(toolbar)

        progressdialog = ProgressDialog(this)
        super.setContentView(coordinatorLayout)
    }

    fun hideToolbar() {
        toolbar.visibility = View.GONE
    }

    fun setScreenTitle(resId: Int) {
        supportActionBar!!.title = getString(resId)
    }

    fun enableBack() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    fun disableBack() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
    }


    fun showProgressDialog() {
        if (mProgressDialog.visibility == View.GONE) {
            mProgressDialog.visibility = View.VISIBLE
        }

    }

    fun dismissProgressDialog() {
        mProgressDialog.visibility = View.GONE

    }

    fun showProgressDialog(message: String) {

        progressdialog.setMessage(message)
        progressdialog.show()
    }

    fun dismissProgressDialogPopup() {
        if (progressdialog.isShowing) {
            progressdialog.dismiss()
        }
    }

    fun showMessage(context: Context, message: String) {

        if (::messageDialog.isInitialized) {
            if (messageDialog != null && messageDialog.isShowing) {
                messageDialog.dismiss()
            }
        }
        messageDialog = Dialog(
            context,
            0
        )

        messageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        messageDialog.setContentView(R.layout.layout_message)

        messageDialog.window!!.attributes.windowAnimations = R.style.PopDialogAnimation
        messageDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val lp = messageDialog.window!!.attributes
        lp.gravity = Gravity.BOTTOM
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        lp.dimAmount = 0.7f
        messageDialog.window!!.attributes = lp

        messageDialog.window!!.attributes.dimAmount = 0.4f
        messageDialog.findViewById<TextView>(R.id.tv_message).text = message
        messageDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {

            messageDialog.dismiss()


        }
        messageDialog.setCancelable(true)
        messageDialog.show()

    }

    fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    fun createCustomDialog(
        context: Context,
        layoutId: Int,
        animationStyleId: Int,
        isBlureBehind: Boolean,
        cancelable: Boolean?,
        isBottom: Boolean
    ): Dialog {

        val dialog = Dialog(
            context,
            0
        )

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.setContentView(layoutId)

        dialog.window!!.attributes.windowAnimations = animationStyleId
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        if (isBottom) {

            val lp = dialog.window!!.attributes
            lp.gravity = Gravity.BOTTOM
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.MATCH_PARENT
            lp.dimAmount = 0.7f
            dialog.window!!.attributes = lp

        }



        if (isBlureBehind)
            dialog.window!!.attributes.dimAmount = 0.4f

        dialog.setCancelable(cancelable!!)

        return dialog

    }
}
