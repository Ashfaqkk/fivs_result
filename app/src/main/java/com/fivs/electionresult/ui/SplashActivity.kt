package com.fivs.electionresult.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.fivs.electionresult.R
import kotlinx.coroutines.*

class SplashActivity : AppCompatActivity() {

    private val delayTimeAfterApi: Long = 1500

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        delaySplashScreen()
    }


    private fun delaySplashScreen() = GlobalScope.launch {

        withContext(Dispatchers.Default) { delay(delayTimeAfterApi) }
        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
        finish()
    }

}
