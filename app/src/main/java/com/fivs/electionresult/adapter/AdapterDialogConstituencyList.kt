package com.fivs.electionresult.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fivs.electionresult.callback.ConstituencyClickCallback
import com.fivs.electionadminapp.data.model.ConstituencyModel
import com.fivs.electionresult.R
import kotlinx.android.synthetic.main.item_dialog_state_list.view.*

class AdapterDialogConstituencyList(
    var context: Context,
    private var callback: ConstituencyClickCallback,
    var mList: ArrayList<ConstituencyModel>
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<AdapterDialogConstituencyList.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(context, callback, mList[position])
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_dialog_state_list, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {


        fun onBind(
            context: Context,
            callback: ConstituencyClickCallback,
            data: ConstituencyModel
        ) {



            itemView.tvName.text = data.name
            itemView.root.setOnClickListener {

                callback.onItemClick(position = adapterPosition)

            }


        }
    }

}