package com.fivs.electionresult

import android.app.Application
import com.google.firebase.FirebaseApp

class ElectionResultApp: Application()

{
    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this);

    }

}